/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var map = require('./routes/map');
var http = require('http');
var path = require('path');
var mongoskin = require('mongoskin');

var app = express();


// DB connection      

    // open shift- connection
    var murl = "mongodb://" + process.env.OPENSHIFT_MONGODB_DB_USERNAME +
        ":" + process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
        process.env.OPENSHIFT_MONGODB_DB_HOST + ":" +
        process.env.OPENSHIFT_MONGODB_DB_PORT + "/" +
        process.env.OPENSHIFT_APP_NAME;

    var db = mongoskin.db(murl, {
        safe: true
    });





// mid for intercept collection name
app.param('collectionName', function (req, res, next, collectionName) {
    req.collection = db.collection('root');
    return next()
});
// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/points/:collectionName', function (req, res, next) {
    req.collection.find({}, {
        limit: 10,
        sort: [['_id', -1]]
    }).toArray(function (e, results) {

        if (e) return next(e)
        var data = [
            {
                path: results,
                stroke: {
                    color: '#6060FB',
                    weight: 3
                },
                editable: true,
                draggable: false,
                geodesic: false,
                visible: true
                }

            ]
        res.send(data);
    })
})
app.get('/map', map.show);
app.get('/users', user.list);
app.post('/points/:collectionName', function (req, res, next) {
    console.log("Added now coords" + new Date());
    var data = req.body;
    data.time = new Date();
    req.collection.insert(data, {}, function (e, results) {
        if (e) return next(e)
        console.log(results);
        res.send(results)

    })
});

if (process.env.OPENSHIFT_NODEJS_PORT !== undefined)
{
app.listen(process.env.OPENSHIFT_NODEJS_PORT, process.env.OPENSHIFT_NODEJS_IP, function () {
    console.log('%s: Node server started on %s:%d ...');
});
} else {
    http.createServer(app).listen(3000);
}