/*
 * GET home page.
 */
var path = require('path');
exports.index = function (req, res) {

    var requestedView = path.join(__dirname + '/../views/index.html');
    res.sendfile(requestedView);

};