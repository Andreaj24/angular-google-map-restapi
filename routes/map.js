/*
 * GET home page.
 */
var path = require('path');
exports.show = function (req, res) {

    var requestedView = path.join(__dirname + '/../views/example.html');
    res.sendfile(requestedView);

};